import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class PopUpMessage {
	static public void Error(String overview, String message) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error Dialog");
		alert.setHeaderText(overview);
		alert.setContentText(message);

		alert.showAndWait();
	}
	
	static public void Information(String overview, String message) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Information Dialog");
		alert.setHeaderText(overview);
		alert.setContentText(message);

		alert.showAndWait();
	}
}
