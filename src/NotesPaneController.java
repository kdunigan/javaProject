import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.regex.*;

import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.concurrent.Task;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.beans.value.ObservableValue;
import javafx.stage.FileChooser;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class NotesPaneController implements Initializable {

	@FXML
	private TextArea notesArea;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
	public void saveNotes() {
		
		ObservableList<CharSequence> paragraph = notesArea.getParagraphs();
	    Iterator<CharSequence>  iter = paragraph.iterator();
	    FileChooser fileChooser = new FileChooser();
		File selectedFile = fileChooser.showSaveDialog(null);
	    try
	    {
	        BufferedWriter bf = new BufferedWriter(new FileWriter(selectedFile));
	        while(iter.hasNext())
	        {
	            CharSequence seq = iter.next();
	            bf.append(seq);
	            bf.newLine();
	        }
	        bf.flush();
	        bf.close();
	    }
	    catch (Exception e)
	    {
	    	PopUpMessage.Error("Could Not Save", "There was an issue saving the file. Please try again.");
	    }
		
	}
	
	public void openNotes() {
		FileChooser fileChooser = new FileChooser();
		File selectedFile = fileChooser.showOpenDialog(null);
		
		if (selectedFile != null) {
			try {
				FileInputStream stream = new FileInputStream(selectedFile);
				byte[] data = new byte[(int) selectedFile.length()];
				stream.read(data);
				stream.close();
				notesArea.setText(new String(data, "UTF-8"));
			} catch (Exception e) {
				PopUpMessage.Error("Could Not Open", "There was error when opening the file. Please try again.");
			}
		} else {
			PopUpMessage.Error("Could Not Open.", "That file does not exist. Please try opening an existing file.");
		}
	}
}