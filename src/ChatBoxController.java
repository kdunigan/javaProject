import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.*;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.concurrent.Task;
import javafx.application.Platform;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class ChatBoxController implements Initializable {
	@FXML
	private VBox messagePane;
	@FXML
	private Button sendButton;
	@FXML
	private TextArea messageField;

	public static ServerHandler server;

	private final String SEND_COLOR    = "lightblue";
	private final String RECIEVE_COLOR = "yellow";

	public void addMessage(String message, String color) {
		
		try{
			JSONParser parser = new JSONParser();
			JSONObject obj = (JSONObject) parser.parse(message);
			 
			TextArea newMessage = new TextArea();
		 	newMessage.setText(obj.get("from") + ": \n" + obj.get("contents"));
		 	newMessage.setEditable(false);
			newMessage.setStyle("-fx-background-color: " + color + "; -fx-text-fill: black; -fx-font-size: 20;");
			newMessage.setPrefRowCount(1);
			
			messagePane.getChildren().add(newMessage);
	         
	      }catch(ParseException pe){
			
	         System.out.println("position: " + pe.getPosition());
	         System.out.println(pe);
	      }
		
	}
	
	@FXML
	public void sendMessage(ActionEvent event) {
		String message = messageField.getText();
		messageField.setText("");

		try {
			if (CharacterCreatorController.CharName.trim() != "") {
				addMessage(server.sendMessage(message), SEND_COLOR);
			} else {
				PopUpMessage.Information("Message Send Conflict", "Ooops, it appears your character has no name! You must give your character a name before sending messages.");
			}
		} catch (Exception e) {
			PopUpMessage.Error("Message Send Error", "An error occured while sending the message. Please try again.");
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			server = new ServerHandler();
			Task<Void> task = new Task<Void>() {
			    @Override 
			    public Void call() {
			        try {
			        	System.out.println(server);
				        String message;
				      	while((message = server.getMessage()) != null) {
				      		final String mess = message;
				      		Platform.runLater(new Runnable() {
			                     @Override public void run() {
			                    	 System.out.println("Message: " + mess);
			                         addMessage(mess, RECIEVE_COLOR);
			                     }
			                 });
				      		System.out.println(message);
				      	}
				    } catch (Exception e) {

				    }
				    return null;
			    }
			};
			new Thread(task).start();
		} catch (Exception e) {
			
		}
	}

	//TODO: not working
	@Override
	public void finalize() throws Exception {
		server.closeSocket();
	}
}

