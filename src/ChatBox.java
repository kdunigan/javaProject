import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.*;

public class ChatBox extends Application {
	
	
	/**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Application.launch(ChatBox.class, (java.lang.String[])null);
    }

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			FXMLLoader loader = new FXMLLoader(ChatBox.class.getResource("ChatBox.fxml"));
            GridPane page = (GridPane) loader.load();
            Scene scene = new Scene(page);
            primaryStage.setScene(scene);
            primaryStage.setTitle("ChatBox Application");
            primaryStage.setOnCloseRequest(event -> windowCloseEvent());
            primaryStage.show();
        } catch (Exception ex) {
            System.out.println(ex);
            windowCloseEvent();
        }
	}
	
	public void windowCloseEvent() {
		System.exit(0);
	}
	
}
