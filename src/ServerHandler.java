import java.io.*;
import java.net.*;
import org.json.simple.JSONObject;


public class ServerHandler {

    public static final String host = "localhost";
    public static final int    port = 3000;

    private Socket socket           = null;
    private PrintWriter wtr         = null;
    private BufferedReader bufRead  = null;

    /**
     * Constructs the object.
     */
    ServerHandler() throws Exception {
        try {
            testConnection();
        } catch (UnknownHostException e) {
            //kill program with message to let user know server is down
        }
    }

    /**
     * This method is used to make sure that we have a connection
     * to the server and to set up our communication variables.
     *
     * @throws     Exception  Throws exception if server cant be found.
     */
    public void testConnection() throws Exception{
        //Instantiate a new socket
        this.socket = new Socket(host, port);

        //Instantiates a new PrintWriter passing in the sockets output stream
        wtr = new PrintWriter(this.socket.getOutputStream());

        //Creates a BufferedReader that contains the server response
        bufRead = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
    }

    /**
     * Sends a message to the server which will then be sent to all in the chat.
     *
     * @param      message    The message as a string
     *
     * @throws     Exception  Throws exception if server cant be found.
     * @return     Returns the json object being sent
     */
    public String sendMessage(String message) throws Exception {
        //Prints the request string to the output stream
//        wtr.println("m " + message);
//        wtr.flush();
        JSONObject obj = new JSONObject();
        String name = CharacterCreatorController.CharName; //name of character will go here

        obj.put("from", name);
        obj.put("action", "message");
        obj.put("contents", message);
        wtr.println(obj);
        wtr.flush();
        
        return obj.toString();
    }

    /**
     * Gets the next message in the socket buffer.
     *
     * @return     The message as a string.
     *
     * @throws     Exception  Throws exception if server cant be found.
     */
    public String getMessage() throws Exception {
        return bufRead.readLine();
    }

    /**
     * Closes a socket.
     * Call this before closing the application.
     *
     * @throws     Exception  Throws exception if server cant be found.
     */
    public void closeSocket() throws Exception {
    	wtr.println("e ");
    }

}