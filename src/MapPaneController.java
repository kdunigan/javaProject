import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.*;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.concurrent.Task;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;

public class MapPaneController implements Initializable {

	@FXML
	private ImageView mapImage;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
	public void importMap() {
		FileChooser fileChooser = new FileChooser();
		File selectedFile = fileChooser.showOpenDialog(null);
		
		if (selectedFile != null) {
			try {				
				String imgPath = selectedFile.toURI().toURL().toString();
				Image map = new Image(imgPath);
				mapImage.setImage(map);
				
			} catch (Exception e) {
				System.out.println("Error when importing map.");
			}
		} else {
			System.out.println("Failed to import map.");
		}
	}
}