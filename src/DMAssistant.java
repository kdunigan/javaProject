import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.*;
import javafx.fxml.FXML;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;

public class DMAssistant extends Application {
	

	/**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Application.launch(DMAssistant.class, (java.lang.String[])null);
    }

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			FXMLLoader loader = new FXMLLoader(DMAssistant.class.getResource("DMAssistant.fxml"));
            SplitPane page = (SplitPane) loader.load();
            Scene scene = new Scene(page);
            primaryStage.setScene(scene);
            primaryStage.setTitle("DM Assistant");
            primaryStage.getIcons().add(new Image("icon.png"));
            primaryStage.setOnCloseRequest(event -> windowCloseEvent());
            primaryStage.show();
        } catch (Exception ex) {
            System.out.println(ex);
            windowCloseEvent();
        }
	}
	
	public void windowCloseEvent() {
		System.exit(0);
	}
	
}
