import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.regex.*;

import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileReader;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.concurrent.Task;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.collections.FXCollections;
import javafx.beans.value.ObservableValue;
import javafx.stage.FileChooser;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CharacterCreatorController implements Initializable {
	@FXML
	private TextField characterName;
	@FXML
	private TextField characterStrength;
	@FXML
	private TextField characterDexterity;
	@FXML
	private TextField characterConstitution;
	@FXML
	private TextField characterIntelligence;
	@FXML
	private TextField characterWisdom;
	@FXML
	private TextField characterCharisma;
	@FXML 
	private ChoiceBox<String> characterRace;
	@FXML
	private ChoiceBox<String> characterSubRace;
	@FXML
	private ChoiceBox<String> characterSkills;
	@FXML
	private ChoiceBox<String> characterSpells;
	@FXML
	private ChoiceBox<String> characterEquipment;
	
	public static String CharName = "";


	@Override
	public void initialize(URL location, ResourceBundle resources) {		
		try {

			characterRace.getSelectionModel().selectedItemProperty().addListener(
		    	(ObservableValue<? extends String> observable, String oldValue, String newValue) -> changeFields()
		    );
			this.getRace();
			this.changeFields();
			
		} catch (Exception e) {
			
		}
	}
	
	private Connection connect() {
        // SQLite connection string
		String path = "dnd35.db";
        String url = "jdbc:sqlite:" + path;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
	
	public void getAbilities() {
		String abilities = selectFromMonster("abilities")[0];	
		String abilitiesArr[] = abilities.split(",");
		characterStrength.setText(abilitiesArr[0].trim().substring(3));
		characterDexterity.setText(abilitiesArr[1].trim().substring(3));
		characterConstitution.setText(abilitiesArr[2].trim().substring(3));
		characterIntelligence.setText(abilitiesArr[3].trim().substring(3));
		characterWisdom.setText(abilitiesArr[4].trim().substring(3));
		characterCharisma.setText(abilitiesArr[5].trim().substring(3));
	}
	
	public void getSkills() {
        setChoiceBoxOptions(characterSkills, selectFromMonster("skills"));
	}
	
	public void getSpells() {

		setChoiceBoxOptions(characterSpells, selectNameFrom("spell"));
	}
	
	public void getEquipment() {
		setChoiceBoxOptions(characterEquipment, selectNameFrom("equipment"));
	}
	
	public void getRace() {
		setChoiceBoxOptions(characterRace, selectNameFrom("monster"));
	}
	
	public void getSubRace() {
		setChoiceBoxOptions(characterSubRace, selectFromMonster("family"));
	}
	
	public String[] selectNameFrom(String where) {
		String sql = "SELECT * FROM " + where;
		ArrayList<String> items = new ArrayList<String>();
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
        	 
        	 while(rs.next()) {
        		 items.add(rs.getString("name"));
        	 };
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return items.toArray(new String[0]);
	}
	
	public String[] selectFromMonster(String select) {
		String sql = "SELECT * FROM monster where name='" + characterRace.getValue() + "';";
		ArrayList<String> items = new ArrayList<String>();
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
        	 
        	 while(rs.next()) {
        		 items.add(rs.getString(select));
        	 };
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return items.toArray(new String[0]);
	}
	
	public void changeFields() {
		this.getSubRace();
		this.getSkills();
		this.getSpells();
		this.getEquipment();
		this.getAbilities();
	}

	public void saveCharacter() {
		System.out.println("Saving Character..");
        JSONObject obj = new JSONObject();

        obj.put("name", characterName.getText());
	    obj.put("strength", characterStrength.getText());
	    obj.put("dexterity", characterDexterity.getText());
	    obj.put("constitution", characterConstitution.getText());
	    obj.put("intelligence", characterIntelligence.getText());
	    obj.put("wisdom", characterWisdom.getText());
	    obj.put("charisma", characterCharisma.getText());
	    obj.put("race", characterRace.getValue());
	    obj.put("subrace", characterSubRace.getValue());

	    try {
		    StringWriter out = new StringWriter();
		    obj.writeJSONString(out);
		  
		    String jsonText = out.toString();
		    System.out.print(jsonText);
		    PrintWriter writer = new PrintWriter(characterName.getText() + ".char", "UTF-8");
		    writer.println(jsonText);
		    writer.close();
		} catch (Exception e) {
			PopUpMessage.Error("Could Not Save", "There was an error when saving the character. Please try again.");
		}
	}
	
	public void importCharacter() {
		FileChooser fileChooser = new FileChooser();
		File selectedFile = fileChooser.showOpenDialog(null);
		
		if (selectedFile != null) {
			try {
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(new FileReader(selectedFile));
				JSONObject json = (JSONObject) obj;
				importJSONToGUI(json);
			} catch (Exception e) {
				PopUpMessage.Error("Could Not Save", "There was an error when importing the character. Please try again.");
			}
		} else {
			PopUpMessage.Error("Could Not Open.", "That file does not exist. Please try opening an existing file.");
		}
	}
	
	public void importJSONToGUI(JSONObject json) {
		characterName.setText((String) json.get("name"));
		characterStrength.setText((String) json.get("strength"));
		characterDexterity.setText((String) json.get("dexterity"));
		characterConstitution.setText((String) json.get("constitution"));
		characterIntelligence.setText((String) json.get("intelligence"));
		characterWisdom.setText((String) json.get("wisdom"));
		characterCharisma.setText((String) json.get("charisma"));
		characterRace.setValue((String) json.get("race"));
		characterSubRace.setValue((String) json.get("subrace"));
	}

	public void setChoiceBoxOptions(ChoiceBox<String> obj, String[] arr) {
		obj.setItems(FXCollections.observableArrayList(arr));
		obj.getSelectionModel().selectFirst();
	}
	
	public void updateGlobalName() {
		CharName = characterName.getText();
	}
}
