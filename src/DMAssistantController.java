import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.*;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.concurrent.Task;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.*;


public class DMAssistantController implements Initializable {
	@FXML
	private AnchorPane ChatBox;
	@FXML
	private AnchorPane DiceRoller;
	@FXML
	private AnchorPane CharacterCreator;
	@FXML 
	private TabPane tabPane;
	@FXML
	private Tab mapTab;
	@FXML
	private MapPaneController mapPaneController;
	@FXML
	private ImageView mapImage;
	
	//not sure if need this^
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			//add chatbox to main app
			FXMLLoader loader = new FXMLLoader(DMAssistant.class.getResource("ChatBox.fxml"));
			GridPane pane = (GridPane) loader.load();
			AnchorPane.setTopAnchor(pane,0.0);
			AnchorPane.setBottomAnchor(pane,0.0);
			AnchorPane.setLeftAnchor(pane,0.0);
			AnchorPane.setRightAnchor(pane,0.0);
			ChatBox.getChildren().add(pane);
			//add dice roller to main app
			FXMLLoader loader2 = new FXMLLoader(DMAssistant.class.getResource("DiceRoller.fxml"));
			GridPane pane2 = (GridPane) loader2.load();
			AnchorPane.setTopAnchor(pane2,0.0);
			AnchorPane.setLeftAnchor(pane2,0.0);
			AnchorPane.setRightAnchor(pane2,0.0); //pane will float on bottom
			DiceRoller.getChildren().add(pane2);
			//add character creator to main app
			FXMLLoader loader3 = new FXMLLoader(DMAssistant.class.getResource("CharacterCreator.fxml"));
			AnchorPane pane3 = (AnchorPane) loader3.load();
			AnchorPane.setTopAnchor(pane3,0.0);
			AnchorPane.setBottomAnchor(pane3, 0.0);
			AnchorPane.setLeftAnchor(pane3,0.0);
			AnchorPane.setRightAnchor(pane3,0.0);
			CharacterCreator.getChildren().add(pane3);
			//code for resizing map image
			FXMLLoader loader4 = new FXMLLoader(DMAssistant.class.getResource("MapPane.fxml"));
			ImageView mapImage = (ImageView) loader4.load();
			mapImage.fitHeightProperty().bind(tabPane.heightProperty());
			mapImage.fitWidthProperty().bind(tabPane.widthProperty());
			mapImage.setPreserveRatio(false);
		} catch (Exception e) {
			
		}
	}
}