import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.*;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.concurrent.Task;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.collections.FXCollections;

public class DiceRollerController implements Initializable {
	@FXML
	private Button rollButton;
	@FXML 
	private Spinner<Integer> numberOfDiceToRoll;
	@FXML 
	private ChoiceBox<String> typeOfDiceToRoll;
	@FXML
	private TextArea rollResultWindow;
	@FXML
	private Label rollTotal;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			// Value factory.
	        SpinnerValueFactory<Integer> valueFactory = //
	                new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 500, 1);
	 
	        numberOfDiceToRoll.setValueFactory(valueFactory);
	        numberOfDiceToRoll.setEditable(true);

			typeOfDiceToRoll.setItems(FXCollections.observableArrayList(
			    	"D4", "D6", "D8", "D10", "D12", "D20", "D100"
			    )
			);
			typeOfDiceToRoll.getSelectionModel().selectFirst();
		} catch (Exception e) {
			
		}
	}

	@FXML 
	public void rollDie(){
		int numSides = getTypeOfDiceToRoll();
		int numDice = getNumberOfDiceToRoll();

		int[] rollResults = new int[numDice];
		//need to input results to roll result box

		for(int i = 0; i < numDice; i++){
			rollResults[i] = (int)(Math.random() * numSides + 1);
		}

		//setText
		showRollResults(rollResults);
	}

	public int getNumberOfDiceToRoll() {
		return (int)numberOfDiceToRoll.getValue();
	}

	public int getTypeOfDiceToRoll() {
		int numSides = 0;
		String type = typeOfDiceToRoll.getValue();
		if ("D4".equals(type)) 
			numSides = 4;
		else if ("D6".equals(type))
			numSides = 6;
		else if ("D8".equals(type))
			numSides = 8;
		else if ("D10".equals(type))
			numSides = 10;
		else if ("D12".equals(type))
			numSides = 12;
		else if ("D20".equals(type))
			numSides = 20;
		else if ("D100".equals(type))
			numSides = 100;
		return numSides;
	}

	public void showRollResults(int[] rollResults) {
		String results   = "";
		int    rollCount = 1;
		int    sum       = 0;
		for (int roll : rollResults) {
			results += "Roll " + rollCount + ": " + roll + "\n";
			sum += roll;
			rollCount++;
		}
		rollTotal.setText("Total: " + sum);
		rollResultWindow.setText(results);
		// try {
		// 	ChatBoxController.server.sendMessage("Name: Total: " + sum + "\n" + results);
		// } catch (Exception e) {

		// }
	}
}