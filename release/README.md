DM Assistant: 
A desktop client application that will store data for a RPG storyline. This 
will have a text of the story, monster stats, and encounters. The client will 
show each player's basic information, save, attack rolls, current/max hit points
and player names. Player clients will show character sheet, spells and inventory
there will also be messages between clients. There will also be a dice roller. 

Using database

* Character Logs 
* Database storage for character components
    * skills, races, stats, classes, equipment
        
Using Networking

* Messaging between players
* Character sheet display in DM client
    
Using Threading

* Multiple box clients on DM side 
* Dice rolling, chat, and package transferring can run at the same time