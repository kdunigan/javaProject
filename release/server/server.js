//I dont think we will need this now but I'm leaving it just in case.
// var express = require('express');
// var app = express();
// // var ws = require('express-ws')(app);


// app.get('/', function (req, res) {
//   res.json('Hello World!');
//   console.log('hello world');
// })

// // app.ws('/', function(ws, req) {
// //   ws.on('message', function(msg) {
// //     ws.send(msg);
// //   });
// //   console.log('hello world!!!');
// //   ws.on("connection", function (ws) {
// //    console.info("websocket connection open");
// //   });
// // });

// app.listen(3000, function () {
//   console.log('Example app listening on port 3000!');
// })

//opens a socket that our java app can communicate to the server through

var net = require('net');

const HOST = 'localhost';
const PORT = 3000;

//will hold all users sockets in chat room
var clients = [];

// Create a server instance, and chain the listen function to it
// The function passed to net.createServer() becomes the event handler for the 'connection' event
// The sock object the callback function receives UNIQUE for each connection
net.createServer({allowHalfOpen: true}, function(sock) {

    sock.name = sock.remoteAddress + ":" + sock.remotePort;

    clients.push(sock);
    
    // We have a connection - a socket object is assigned to the connection automatically
    console.log('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);
    
    // Add a 'data' event handler to this instance of socket
    sock.on('data', function(data) {
        
        console.log('DATA ' + sock.remoteAddress + ': ' + data);
        // Write the data back to the socket, the client will receive it as data from the server
        // sock.write('You said ' + data);
        //keep connection alive so we can send messages in real time
        sock.setKeepAlive(true);
        processMessage(data.toString(), sock);
    });

    // Add a 'close' event handler to this instance of socket
    sock.on('close', function(data) {
        console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
    });
    
}).listen(PORT, HOST);

console.log('Server listening on ' + HOST +':'+ PORT);

/**
 * Process the message recieved and see what action needs to be taken.
 *
 * @param      {string}  message  The message recieved
 * @param      {socket}  sock     The socket that the message was sent through
 */
function processMessage(message, sock) {

//    var action = message[0];
//    switch (action.trim()) {
//        case 'm':
//            broadcastMessage(message.substring(2), sock);
//            break;
//        case 'e':
//            clients.remove(sock);
//            sock.destroy();
//            console.log('done')
//            break;
//        default:
//            break;
//    }
    jsonStr = message;
    message = JSON.parse(message);
    var action = message['action'];
    switch (action) {
        case 'message':
            broadcastMessage(jsonStr, sock);
            break;
        case 'close':
            clients.remove(sock);
            sock.destroy();
            console.log('done')
            break;
        default:
            break;
    }
}

/**
 * Broadcasts a message to all in the chat room.
 *
 * @param      {string}  message  The message to be sent
 * @param      {socket}  sender   The socket that the message was sent through
 */
function broadcastMessage (message, sender) {
    clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client === sender) return;
      client.write(message);
    });
    // Log it to the server output too
    console.log(message);
}
